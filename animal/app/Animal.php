<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    /**
     * 可以被批量賦值的屬性。
     * 用$fillable來限制那些欄位可以被批量寫入
     *
     * @var array
     */
    protected $table = 'animals';
    protected $fillable = [
        'type_id',
        'name',
        'birthday',
        'area',
        'fix',
        'description',
        'personality',
    ];
}