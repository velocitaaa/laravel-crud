<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'user_name'=>'admin',      
            'email'=>'admin@gmail.com',    // 信箱
            'password'=>bcrypt('admin'),  // 密碼(bcrypt產生亂數顯示)
        ]);
    }
}
