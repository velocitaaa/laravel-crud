<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->bigIncrements('id'); //bigInteger(10) unsigned
            $table->unsignedInteger('type_id')->comment('動物分類'); //int(10) unsigned
            $table->string('name')->comment('動物的暱稱'); //varchar(255)
            $table->date('birthday')->nullable()->comment('生日'); //date NULL
            $table->string('area')->nullable()->comment('所在地區'); //varchar(255) NULL	
            $table->boolean('fix')->default(false)->comment('結紮情形'); //tinyint(1)
            $table->text('description')->nullable()->comment('簡單敘述'); //text NULL	
            $table->text('personality')->nullable()->comment('動物個性'); //text NULL	
            $table->timestamps();
        });
    }

    // unsignedInteger:無號整數（unsigned integer）的範圍介於0 和正無窮大之間。其值為(2n – 1),n=16，沒有負數的整數

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
}
