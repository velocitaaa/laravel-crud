<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes / Route轉發要求給Controller
|--------------------------------------------------------------------------
|
| Controller的目的就是為了要處理要求
| 所以我們要在Route中定義要交哪個Controller的什麼方法來處理要求
| 讓我們看看常見的作法
***********
| GET product的要求轉發給ProductController的index方法處理
| Route::get('product', 'ProductController@index');
***********
| GET product{id}的要求轉發給ProductController的show方法處理，同時會傳遞參數id
| Route::get('product/{id}', 'ProductController@show');
***********
| POST product的要求轉發給ProductController的store方法處理
| Route::post('product', 'ProductController@store');
***********
| 另外，你可已透過下列的方式，轉發所有CRUD要求給指定的Controller
| Route::resource('product', 'ProductController');
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::apiResource('animal', 'AnimalController');
Route::apiResource('users', 'UsersController');

// Route::resource('users', 'UsersController@update');



// 可以排除不需要使用的方法
// Route::resource('product', 'ProductController', ['only' => [
//     'index', 'show'
// ]]);
// Route::resource('product', 'ProductController', ['except' => [
//     'create', 'store', 'update', 'destroy'
// ]]);